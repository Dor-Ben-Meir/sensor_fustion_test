#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct {
    double state[2];           // State vector [position, velocity]
    double covariance[2][2];   // Covariance matrix
    double process_noise[2][2]; // Process noise covariance matrix
    double measurement_noise;  // Measurement noise covariance for velocity measurements
} EKF;


double derivative(double position, double velocity) {
    return velocity; 
}

// RK4 integration method
double rk4(double y, double dydt, double dt) {
    double k1 = dt * derivative(y, dydt);
    double k2 = dt * derivative(y + 0.5 * k1, dydt);
    double k3 = dt * derivative(y + 0.5 * k2, dydt);
    double k4 = dt * derivative(y + k3, dydt);
    return y + (k1 + 2 * k2 + 2 * k3 + k4) / 6.0;
}

void init_ekf(EKF *ekf, double initial_position, double initial_velocity, double initial_position_cov, double initial_velocity_cov, double process_noise_vel) {
    ekf->state[0] = initial_position;
    ekf->state[1] = initial_velocity;
    ekf->covariance[0][0] = initial_position_cov;
    ekf->covariance[0][1] = ekf->covariance[1][0] = 0.0;
    ekf->covariance[1][1] = initial_velocity_cov;
    // ekf->process_noise[0][0] = 0.0; 
    ekf->process_noise[0][0] = 0.001; 
    ekf->process_noise[0][1] = ekf->process_noise[1][0] = 0.0;
    ekf->process_noise[1][1] = process_noise_vel; // Process noise for velocity
    
}

void predict_ekf(EKF *ekf, double dt) {
    // Integrate position using RK4
    ekf->state[0] = rk4(ekf->state[0], ekf->state[1], dt);
    ekf->state[1] = ekf->state[1];  // Velocity 

    // Update covariance matrix 
    double F[2][2] = {{1.0, dt}, {0.0, 1.0}};  // State transition matrix
    double temp_cov[2][2];
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            temp_cov[i][j] = 0;
            for (int k = 0; k < 2; ++k) {
                temp_cov[i][j] += F[i][k] * ekf->covariance[k][j];
            }
        }
    }
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            ekf->covariance[i][j] = 0;
            for (int k = 0; k < 2; ++k) {
                ekf->covariance[i][j] += temp_cov[i][k] * F[j][k];
            }
        }
    }
    // Add process noise
    ekf->covariance[0][0] += ekf->process_noise[0][0] * dt;
    ekf->covariance[1][1] += ekf->process_noise[1][1] * dt * dt;
}


void update_ekf(EKF *ekf, double measurement, double measurement_noise, int which_vel) {
    double H[2] = {0.0, 1.0};  // Measurement matrix
    double S = H[0] * ekf->covariance[0][0] * H[0] + H[0] * ekf->covariance[0][1] * H[1] +
               H[1] * ekf->covariance[1][0] * H[0] + H[1] * ekf->covariance[1][1] * H[1] +
               measurement_noise;
    double K[2];
    K[0] = (ekf->covariance[0][0] * H[0] + ekf->covariance[0][1] * H[1]) / S;
    K[1] = (ekf->covariance[1][0] * H[0] + ekf->covariance[1][1] * H[1]) / S;
    double y = measurement - (H[0] * ekf->state[0] + H[1] * ekf->state[1]);  // Measurement residual


    ekf->state[1] += K[1] * y;


    if(which_vel){
    ekf->state[0] += K[0] * y;
    }

    double I[2][2] = {{1.0, 0.0}, {0.0, 1.0}};
    double temp_I_KH[2][2];
    temp_I_KH[0][0] = I[0][0] - K[0] * H[0];
    temp_I_KH[0][1] = I[0][1] - K[0] * H[1];
    temp_I_KH[1][0] = I[1][0] - K[1] * H[0];
    temp_I_KH[1][1] = I[1][1] - K[1] * H[1];

    double new_cov[2][2];
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            new_cov[i][j] = 0;
            for (int k = 0; k < 2; ++k) {
                new_cov[i][j] += temp_I_KH[i][k] * ekf->covariance[k][j];
            }
        }
    }

    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            ekf->covariance[i][j] = new_cov[i][j];
        }
    }
}

// gcc -shared -o libekf_rk4.so -fPIC ExtendedKalmanFilter_rq4.c

import ctypes
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.integrate import cumtrapz

# Define the EKF structure as expected by ctypes
class EKF(ctypes.Structure):
    _fields_ = [
        ("state", ctypes.c_double * 2),
        ("covariance", ctypes.c_double * 4),  # Flattened 2x2 matrix
        ("process_noise", ctypes.c_double * 4), # Flattened 2x2 matrix
        ("measurement_noise", ctypes.c_double)
    ]

# Load the shared library
ekf_lib = ctypes.CDLL('./libekf_rk4.so')

# Specify the argument and return types for C functions
ekf_lib.init_ekf.argtypes = [
    ctypes.POINTER(EKF), 
    ctypes.c_double, ctypes.c_double,  # Initial state: position, velocity
    ctypes.c_double, ctypes.c_double,  # Initial covariance: pos_var, vel_var
    ctypes.c_double                   # Process noise for velocity
]
ekf_lib.predict_ekf.argtypes = [ctypes.POINTER(EKF), ctypes.c_double]
ekf_lib.update_ekf.argtypes = [ctypes.POINTER(EKF), ctypes.c_double, ctypes.c_double, ctypes.c_int]

# Initialize the EKF instance
ekf_instance = EKF()
initial_position = 0.0
initial_velocity = 0.0
initial_position_cov = 1.0
initial_velocity_cov = 1.0
process_noise_velocity = 0.01
measurement_noise_sensor2 = 0.25  # Measurement noise (variance) of sensor 2
measurement_noise_sensor1 = 1.0  # Measurement noise (variance) of sensor 1
ekf_lib.init_ekf(ctypes.byref(ekf_instance), initial_position, initial_velocity, initial_position_cov, initial_velocity_cov, process_noise_velocity)


# Read the CSV file into a DataFrame
df = pd.read_csv('./Velocity sensors datacsv.csv')

# Convert time from microseconds to seconds
df['time (us)'] = ((df['time (us)'] - df['time (us)'][0])/ 1e6) 

# Convert velocities from cm/s to m/s
df['flowVelocityX[cm/s]'] = df['flowVelocityX[cm/s]'] / 100
df['secFlowVelocityX[cm/s]'] = df['secFlowVelocityX[cm/s]'] / 100

# Convert the columns to NumPy arrays
times = np.array(df['time (us)'])
flow_velocity_x_array = np.array(df['flowVelocityX[cm/s]'])
sec_flow_velocity_x_array = np.array(df['secFlowVelocityX[cm/s]'])

# Numpy array of the two sensors
vel_x_1 = flow_velocity_x_array
vel_x_2 = sec_flow_velocity_x_array


# Positions initialized at zero
positions_1 = np.zeros_like(vel_x_1)
positions_2 = np.zeros_like(vel_x_2)
ekf_positions = np.zeros_like(vel_x_1)


# Use EKF for estimates
# estimates = []
positions = [initial_position]
velocities = [initial_velocity]
last_time = times[0]
for (time, meas1, meas2) in (zip(times, vel_x_1, vel_x_2)):
    dt = time - last_time
    ekf_lib.predict_ekf(ctypes.byref(ekf_instance), dt)
    ekf_lib.update_ekf(ctypes.byref(ekf_instance), meas1, measurement_noise_sensor1, 1)
    ekf_lib.update_ekf(ctypes.byref(ekf_instance), meas2, measurement_noise_sensor2, 1)
    positions.append(ekf_instance.state[0])
    velocities.append(ekf_instance.state[1])
    last_time = time


position_x_data = cumtrapz(flow_velocity_x_array, times, initial=0)
sec_position_x = cumtrapz(sec_flow_velocity_x_array, times, initial=0)


# Plot the results
plt.figure(figsize=(10, 5))
plt.subplot(2, 1, 1)
plt.plot(times, vel_x_1, label='Sensor 1 Measurements', color='lightblue')
plt.plot(times, vel_x_2, label='Sensor 2 Measurements', color='lightgreen')
plt.plot(times, velocities[1:], label='Estimated Velocities', color='red')
plt.xlabel('Time [s]')
plt.ylabel('Velocity [m/sec]')
plt.title('Velocity Estimation')
plt.legend()
plt.grid(True)

plt.subplot(2, 1, 2)
plt.plot(times, position_x_data, label='Velocity_1_Integration', color='lightblue')
plt.plot(times, sec_position_x, label='Velocity_2_Integration', color='lightgreen')
plt.plot(times, positions[1:], label='Estimated Positions', color='red')
plt.title('Position Estimation')
plt.xlabel('Time (s)')
plt.ylabel('Position [m]')
plt.legend()
plt.grid(True)
# plt.tight_layout()
plt.savefig('/Share_Host/Biduls/x-test/XTEND_fig.png')

plt.show()

  
  
  
# Dor Ben-Meir - Flight Control Engineer Test 
  
  
>- In this specific application, an EKF is configured to estimate both the position and velocity of a drone, moving along the X direction.
>>- Given 2 sensor measurements that only observe the velocity in the x direction with different noises.
>>> - Positions calculated using the fourth-order Runge-Kutta (RK4) method. 
  
## Mathematical Theory:
### State Vector
  
The state vector <img src="https://latex.codecogs.com/gif.latex?\mathbf{x}"/> includes the position and velocity of the drone:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?\mathbf{x}%20=%20\begin{bmatrix}\text{position}%20\\\text{velocity}\end{bmatrix}"/></p>  
  
  
### State Transition Model
  
The state transition model predicts the next state based on the current state and the elapsed time <img src="https://latex.codecogs.com/gif.latex?dt"/>, assuming constant velocity during the interval:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?\mathbf{x}_{k+1}%20=%20\mathbf{F}_k%20\mathbf{x}_k%20+%20\mathbf{w}_k"/></p>  
  
  
where <img src="https://latex.codecogs.com/gif.latex?\mathbf{F}_k"/> is the state transition matrix:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?\mathbf{F}_k%20=%20\begin{bmatrix}1%20&amp;%20dt%20\\0%20&amp;%201\end{bmatrix}"/></p>  
  
  
and <img src="https://latex.codecogs.com/gif.latex?\mathbf{w}_k"/> is the process noise, which is assumed to be Gaussian.
  
### Process Noise Covariance
  
The process noise covariance matrix <img src="https://latex.codecogs.com/gif.latex?\mathbf{Q}"/> models the uncertainties in the process:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?\mathbf{Q}%20=%20\begin{bmatrix}q_{pos}%20\times%20dt%20&amp;%200%20\\0%20&amp;%20q_{vel}%20\times%20dt^2\end{bmatrix}"/></p>  
  
  
where <img src="https://latex.codecogs.com/gif.latex?q_{pos}"/> and <img src="https://latex.codecogs.com/gif.latex?q_{vel}"/> represent the process noise power for the position and velocity states, respectively.
  
### Measurement Model
  
The measurement vector <img src="https://latex.codecogs.com/gif.latex?\mathbf{z}"/> only observes the velocity:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?\mathbf{z}_k%20=%20\mathbf{H}_k%20\mathbf{x}_k%20+%20\mathbf{v}_k"/></p>  
  
  
where <img src="https://latex.codecogs.com/gif.latex?\mathbf{H}_k"/> is the measurement matrix:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?\mathbf{H}_k%20=%20\begin{bmatrix}0%20&amp;%201\end{bmatrix}"/></p>  
  
  
and <img src="https://latex.codecogs.com/gif.latex?\mathbf{v}_k"/> is the measurement noise, assumed to be Gaussian.
  
### Measurement Noise Covariance
  
The measurement noise covariance <img src="https://latex.codecogs.com/gif.latex?\mathbf{R}"/> is:
  
<p align="center"><img src="https://latex.codecogs.com/gif.latex?\mathbf{R}%20=%20\begin{bmatrix}r_{vel_a1/2}\end{bmatrix}"/></p>  
  
  
where <img src="https://latex.codecogs.com/gif.latex?r_{vel_a1/2}"/> represents the measurement noise power for each of the velocities measurements.
  
### Prediction Phase
  
1. **Predict the state**:
   <p align="center"><img src="https://latex.codecogs.com/gif.latex?\mathbf{\hat{x}}_{k|k-1}%20=%20\mathbf{F}_k%20\mathbf{\hat{x}}_{k-1|k-1}"/></p>  
  
  
2. **Predict the covariance**:
   <p align="center"><img src="https://latex.codecogs.com/gif.latex?\mathbf{P}_{k|k-1}%20=%20\mathbf{F}_k%20\mathbf{P}_{k-1|k-1}%20\mathbf{F}_k^T%20+%20\mathbf{Q}_k"/></p>  
  
  
### Update Phase
  
1. **Compute the Kalman Gain**:
   <p align="center"><img src="https://latex.codecogs.com/gif.latex?\mathbf{K}_k%20=%20\mathbf{P}_{k|k-1}%20\mathbf{H}_k^T%20(\mathbf{H}_k%20\mathbf{P}_{k|k-1}%20\mathbf{H}_k^T%20+%20\mathbf{R}_k)^{-1}"/></p>  
  
  
2. **Update the estimate twice for each sensor measurement**:
   <p align="center"><img src="https://latex.codecogs.com/gif.latex?\mathbf{\hat{x}}_{k|k}%20=%20\mathbf{\hat{x}}_{k|k-1}%20+%20\mathbf{K}_k%20(\mathbf{z}_k%20-%20\mathbf{H}_k%20\mathbf{\hat{x}}_{k|k-1})"/></p>  
  
  
3. **Update the covariance**:
   <p align="center"><img src="https://latex.codecogs.com/gif.latex?\mathbf{P}_{k|k}%20=%20(\mathbf{I}%20-%20\mathbf{K}_k%20\mathbf{H}_k)%20\mathbf{P}_{k|k-1}"/></p>  
  
  
## EKF Initialization Parameters
  
| Parameter             | Symbol         | Description                                                  | Value |
|-----------------------|----------------|--------------------------------------------------------------|---------------------|
| Initial Position      | <img src="https://latex.codecogs.com/gif.latex?x_{pos}"/>    | The initial estimate of the drone's position.               | 0 m                 |
| Initial Velocity      | <img src="https://latex.codecogs.com/gif.latex?x_{vel}"/>    | The initial estimate of the drone's velocity.               | 0 m/s              |
| Initial Position Covariance | <img src="https://latex.codecogs.com/gif.latex?P_{pos}"/> | Initial covariance of the position estimate.               | 1.0                 |
| Initial Velocity Covariance | <img src="https://latex.codecogs.com/gif.latex?P_{vel}"/> | Initial covariance of the velocity estimate.               | 1.0                 |
| Process Noise for Position | <img src="https://latex.codecogs.com/gif.latex?Q_{pos}"/> | Position process noise variance.        | 0.001 |
| Process Noise for Velocity | <img src="https://latex.codecogs.com/gif.latex?Q_{vel}"/> | Process noise variance affecting velocity.                 | 0.01                |
| Measurement Noise1     | <img src="https://latex.codecogs.com/gif.latex?R_{vel_asensor1}"/>          | Variance of the measurement noise in sensor one. | 1.0                |
| Measurement Noise2     | <img src="https://latex.codecogs.com/gif.latex?R_{vel_asensor2}"/>          | Variance of the measurement noise in sensor two. | 0.25                |
  
  
  
## Results:
<p align="center">
<img src="./doc/Figure_1.png"/>
  